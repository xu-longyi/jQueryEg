//js代码

//访问main的div
var div = document.getElementById("main");
//访问ul的节点
var ul = div.firstElementChild;
document.write("ul节点："+ul);
//输出No.1文本
var v = ul.firstElementChild.firstChild.nodeValue;
document.write("<br/>节点的值是："+v);
//文本的节点类型
var vtype = ul.firstElementChild.firstChild.nodeType;
document.write("<br/>文本的节点类型是："+vtype);
//文本的节点名称
var vname = ul.firstElementChild.firstChild.nodeName;
document.write("<br/>文本的节点名称是："+vname);

//元素的节点名称，li的节点名称
var liname = ul.firstElementChild.nodeName;
document.write("<br/>li的节点名称是："+liname);

//元素的节点类型,li的节点类型
var litype = ul.firstElementChild.nodeType;
document.write("<br/>元素的节点类型是："+litype);

//获取NO.2的文本值
var li2 = ul.firstElementChild.nextElementSibling.nodeValue;
var li22 = ul.lastElementChild.previousElementSibling.previousElementSibling.nodeValue;

///////操作节点/////////

//创建节点（info）
var inp = document.createElement("input");
//设置节点的属性
inp.setAttribute("type","text");
inp.setAttribute("value","王炎霸");
//插入节点
var info =  document.getElementById("info");
info.appendChild(inp);

//在输入框的前面插入一个超链接
var a  = document.createElement("a");
a.innerHTML = "超链接";
a.setAttribute("href","chop3.html");
info.insertBefore(a,inp);

//删除节点
info.removeChild(a);

//克隆节点
var newinp = inp.cloneNode(true);
info.appendChild(newinp);

//用其他节点替换指定节点（用按钮来替换文本框）
var btn = document.createElement("button");
btn.innerHTML = "范啸天按钮";
info.replaceChild(btn,inp);

//设置No.1的颜色为红色
ul.firstElementChild.style.color = "red";
//设置No.2的背景颜色为橘色
ul.firstElementChild.nextElementSibling.style.background = "orange";
//设置最后一个li的字体大小为30px;
ul.lastElementChild.style.fontSize = "30px";

//新建一个div
var div3 = document.createElement("div");
div3.setAttribute("id","div3");
div3.style.width = "100px";
div3.style.height = "100px";
div3.style.background = "greenyellow";
div.appendChild(div3);

//当鼠标悬浮div3的时候，infodiv隐藏
div3.setAttribute("onmouseover","test()");
div3.setAttribute("onmouseout","test1()");


function test(){
	//1秒之内影藏
	info.style.display = "none";
}

function test1(){
	info.style.display = "block";
}

//使用js给main的div添加样式表
div.className = "main";

//获取元素的样式(只适应与ie浏览器)
//document.write("<br/>main的div的宽是："+div.currentStyle.width);
//在除ie浏览器之外回去元素的属性（main的div的宽）
var w = document.defaultView.getComputedStyle(div,null).width;
document.write("<br/>谷歌浏览器中获取宽："+w);

//获取元素的位置
//获取当前元素与父级元素的左边距离
document.write("<br/>左边距离是："+div.offsetLeft);
document.write("<br/>上边距离是："+div.offsetTop);
document.write("<br/>元素的高度是："+div.offsetHeight);
document.write("<br/>元素的宽度是："+div.offsetWidth);
document.write("<br/>元素与滚动条的垂直位置："+div.scrollTop);
document.write("<br/>元素与滚动条的水平位置：："+div.scrollLeft);
document.write("<br/>元素的可见高度是："+div.clientHeight);
document.write("<br/>元素的可见宽度是："+div.clientWidth);
